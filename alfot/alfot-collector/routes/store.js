const store = module.exports = {
  rawData: new Map(),
  receivers: new Map(),
  nextId: () => store._id++,
  _id: 1,
};
