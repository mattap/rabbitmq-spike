const config = require('config');
const Broker = require('./broker');

const CHANNEL_EXCHANGE =
  process.env.CHANNEL_EXCHANGE || config.get('broker.exchange');
const COLLECTOR_QUEUE =
  process.env.COLLECTOR_QUEUE || config.get('broker.queue');

class Consumer extends Broker {
  constructor() {
    super();
    this._handlers = {};
    this._consuming = false;
    this._connected = false;
    this._bindQueue = [];
  }

  async _messageHandler(msg) {
    // Get all handlers registered for the given routing key
    const handlers = this._handlers[msg.fields.routingKey] || [];
    // Make sure there are handlers for the given routing key
    if (!handlers.length) {
      return;
    }

    // Dispatch all handlers with the message routing key and payload
    await Promise.all(handlers.map(async (handler) => handler(
      msg.fields.routingKey,
      JSON.parse(msg.content.toString()),
    )));
    // If at least one handler succeeded, acknowledge the message
    this.channel.ack(msg);
  }

  _bind({ key, handler }) {
    // If the consumer has not been connected yet
    if (!this._connected) {
      // Add the bind request to the `_bindQueue` and return
      this._bindQueue.push({ key, handler });
      return;
    }
    // Bind the queue to the
    this.channel.bindQueue(this.queue.queue, CHANNEL_EXCHANGE, key);
    // Add to consumption handlers
    this._consume({ key, handler });
  }

  _consume({ key, handler }) {
    // Get existing handlers for the given key, or an empty array if none exists
    const handlers = this._handlers[key] || [];
    // Append the new handlers to the list of registered handlers
    this._handlers[key] = [...handlers, handler];

    // Ensure consumption is called only once
    if (this._consuming) {
      return;
    }
    this._consuming = true;
    this.channel.consume(this.queue.queue, this._messageHandler.bind(this), {
      // All messages must be ackowleged
      noAck: false,
    });
  }

  async connect() {
    await super.connect();
    this.queue = await this.channel.assertQueue(COLLECTOR_QUEUE, {
      durable: true,
    });
    this._connected = true;
    // After connecting bind all pending bind requests
    this._bindQueue.forEach(request => this._bind(request));
    this._bindQueue = [];
  }

  bindReceiverCreated(handler) {
    this._bind({ key: 'receiver.created', handler });
  }

  bindReceiverUpdated(handler) {
    this._bind({ key: 'receiver.updated', handler });
  }

  bindReceiverRemoved(handler) {
    this._bind({ key: 'receiver.removed', handler });
  }

  bindLakeRemoved(handler) {
    this._bind({ key: 'lake.removed', handler });
  }
}

module.exports = new Consumer();
