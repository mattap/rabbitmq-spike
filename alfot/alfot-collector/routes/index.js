const express = require('express');
const store = require('./store');
const consumer = require('./consumer');

const router = module.exports = express.Router();
router.broker = consumer;

// - Setup consumer handlers

consumer.bindReceiverCreated(async (routeKey, data) => {
  console.log('Received Created handler', routeKey, data);
  const { id, lakeId, key } = data;
  store.receivers.set(id, { id, lakeId, key });
});

consumer.bindReceiverUpdated(async (routeKey, data) => {
  console.log('Received Updated handler', routeKey, data);
  const { id, lakeId, key } = data;
  store.receivers.set(id, { id, lakeId, key });
});

consumer.bindReceiverRemoved(async (routeKey, data) => {
  console.log('Received Removed handler', routeKey, data);
  const { id } = data;
  store.receivers.delete(id);
});

consumer.bindLakeRemoved(async (routeKey, data) => {
  console.log('Lake Remved handler', routeKey, data);
  const { id } = data;
  // Iterate through all of the receivers
  Array.from(store.receivers.values())
    // Filter receivers that are associated with the removed lake
    .filter(({ lakeId }) => lakeId === id)
    // Remove all of them
    .forEach(({ id }) => store.receivers.delete(id));
});

router.get('/', (req, res) => {
  res.send('Hello from Collector');
});

/**
 * Middleware creator.
 *
 * Make sure entity `entityKey` with id passed as parameter `paramKey` exists in
 * the store.
 *
 * Return `404 Not Found` if no such found.
 */
function entityByIdMiddleware(entityKey, paramKey) {
  return async (req, res, next) => {
    const id = parseInt(req.params[paramKey]);
    const entity = store[entityKey].get(id);
    if (!entity) {
      return res.status(404).send('Not Found');
    }
    req.params[paramKey] = id;
    next();
  };
}

/**
 * Middleware creator.
 *
 * Validates that token assigned to the requests corresponds to the token of
 * the registered receiver.
 *
 * Return `403 Forbidden` if no match found.
 */
function validateTokenMiddleware() {
  return async (req, res, next) => {
    const { key } = req.body;
    const receiver = store.receivers.get(req.params.receiverId);
    if (key !== receiver.key) {
      return res.status(403).send('Forbidden');
    }
    next();
  };
}

// - /receiver/:receiverId/data Routes

router.route('/receiver/:receiverId/data')
.all(entityByIdMiddleware('receivers', 'receiverId'))
.get(async (req, res) => {
  const data = Array.from(store.rawData.values())
    .filter(datum => datum.receiverId === req.body.receiverId);
  res.send(data);
})
.post(validateTokenMiddleware(), async (req, res) => {
  const id = store.nextId();
  const datum = { id, ...req.body, timestamp: new Date().toISOString() };
  store.rawData.set(id, datum);
  res.status(201).send(datum);
});

// - /receiver Routes

router.route('/receiver')
.get((req, res) => {
  res.send(Array.from(store.receivers.values()));
})
.post((req, res) => {
  const id = parseInt(req.body.id);
  // Make sure id is a number
  const receiver = { ...req.body, id };
  store.receivers.set(id, receiver);
  res.status(201).send(receiver);
});

// - /receiver/:id Routes

router.route('/receiver/:receiverId')
.all(entityByIdMiddleware('receivers', 'receiverId'))
.get((req, res) => {
  res.send(store.receivers.get(req.params.receiverId));
})
.put((req, res) => {
  const receiver = req.body;
  store.receivers.set(req.params.receiverId, receiver);
  res.send(receiver);
})
.delete((req, res) => {
  store.receivers.delete(req.params.receiverId);
  res.sendStatus(204);
});
