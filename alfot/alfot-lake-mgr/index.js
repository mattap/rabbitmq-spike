const config = require('config');
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const routes = require('./routes');

const SERVICE_NAME = process.env.SERVICE_NAME || config.get('service.name');
const PORT = process.env.PORT || config.get('service.port');

async function bootstrap() {
  await routes.broker.connect();
  const app = express();
  app.use(morgan('dev'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(routes);

  try {
    await app.listen(PORT);
    console.info(`${SERVICE_NAME} listening on port ${PORT}`);
  } catch (error) {
    console.error(error);
  }

  // Gracefull cancellation
  let graceful = true;
  process.on('SIGINT', async () => {
    if (graceful) {
      graceful = false;
      await routes.broker.close();
      process.exit(0);
    } else {
      process.exit(1);
    }
  });
}
bootstrap();
