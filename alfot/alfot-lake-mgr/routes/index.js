const express = require('express');
const store = require('./store');
const publisher = require('./publisher');

const controller = module.exports = express.Router();
controller.broker = publisher;

controller.get('/', (req, res) => {
  res.send('Hello from Lake Mgr');
});

/**
 * Middleware creator.
 *
 * Make sure entity `entityKey` with id passed as parameter `paramKey` exists in
 * the store.
 *
 * Return `404 Not Found` if no such found.
 */
function entityByIdMiddleware(entityKey, paramKey) {
  return async (req, res, next) => {
    const id = parseInt(req.params[paramKey]);
    const entity = store[entityKey].get(id);
    if (!entity) {
      return res.status(404).send('Not Found');
    }
    req.params[paramKey] = id;
    next();
  };
}

// - /lake Routes

controller.route('/lake')
.get(async (req, res) => {
  res.send(Array.from(store.lakes.values()));
})
.post(async (req, res) => {
  const id = store.nextId();
  const lake = { id, ...req.body };
  store.lakes.set(id, lake);
  res.status(201).send(lake);
});

// - /lake/:id Routes

controller.route('/lake/:lakeId')
.all(entityByIdMiddleware('lakes', 'lakeId'))
.get((req, res) => {
  res.send(store.lakes.get(req.params.lakeId));
})
.put((req, res) => {
  const lake = Object.assign({}, store.lakes.get(req.params.lakeId), req.body);
  store.lakes.set(req.params.lakeId, lake);
  res.send(lake);
})
.delete((req, res) => {
  const lake = store.lakes.get(req.params.lakeId);
  store.lakes.delete(req.params.lakeId);

  publisher.publishLakeRemoved(lake);
  res.sendStatus(204);
});


// - /lake/:id/receiver Routes

controller.route('/lake/:lakeId/receiver')
.all(entityByIdMiddleware('lakes', 'lakeId'))
.get((req, res) => {
  const receivers = Array.from(store.receivers.values())
    .filter(receiver => receiver.lakeId === req.params.lakeId);
  res.send(receivers);
})
.post((req, res) => {
  const lakeId = req.params.lakeId;
  const id = store.nextId();
  const receiver = { id, lakeId, ...req.body };
  store.receivers.set(id, receiver);

  publisher.publishReceiverCreated(receiver);
  res.status(201).send(receiver);
});

// - /lake/:id/receiver/:id Routes

controller.route('/lake/:lakeId/receiver/:receiverId')
.all(entityByIdMiddleware('lakes', 'lakeId'))
.all(entityByIdMiddleware('receivers', 'receiverId'))
.get((req, res) => {
  res.send(store.receivers.get(req.params.receiverId));
})
.put((req, res) => {
  const receiver =
    Object.assign({}, store.receivers.get(req.params.lakeId), req.body);
  store.receivers.set(req.params.receiverId, receiver);

  publisher.publishReceiverUpdated(receiver);
  res.send(receiver);
})
.delete((req, res) => {
  const receiver = store.receivers.get(req.params.receiverId);
  store.receivers.delete(req.params.receiverId);

  publisher.publishReceiverRemoved(receiver);
  res.sendStatus(204);
});
