const store = module.exports = {
  lakes: new Map(),
  receivers: new Map(),
  nextId: () => store._id++,
  _id: 1,
};
