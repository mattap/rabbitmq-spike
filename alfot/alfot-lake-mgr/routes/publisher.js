const config = require('config');
const Broker = require('./broker');

const CHANNEL_EXCHANGE =
  process.env.CHANNEL_EXCHANGE || config.get('broker.exchange');

class Publisher extends Broker {
  _bufferify(obj) {
    return new Buffer(JSON.stringify(obj));
  }

  _publish(key, obj) {
    const payload = new Buffer(JSON.stringify(obj));
    this.channel.publish(CHANNEL_EXCHANGE, key, payload);
  }

  publishReceiverCreated(receiver) {
    this._publish('receiver.created', receiver);
  }

  publishReceiverUpdated(receiver) {
    this._publish('receiver.updated', receiver);
  }

  publishReceiverRemoved(receiver) {
    this._publish('receiver.removed', receiver);
  }

  publishLakeRemoved(lake) {
    this._publish('lake.removed', lake);
  }
}

module.exports = new Publisher();
