const amqp = require('amqplib');
const config = require('config');

const BROKER_URL = process.env.BROKER_URL || config.get('broker.url');
const CHANNEL_EXCHANGE =
  process.env.CHANNEL_EXCHANGE || config.get('broker.exchange');

class Broker {
  async connect() {
    this.connection = await amqp.connect(BROKER_URL);
    this.channel = await this.connection.createChannel();
    // Make sure the `topic` exchange is setup
    this.channel.assertExchange(CHANNEL_EXCHANGE, 'topic', {
      durable: true,
    });
  }

  async close() {
    await this.channel.close();
    await this.connection.close();
  }
}

module.exports = Broker;
