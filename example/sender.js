#!/usr/bin/env node

const amqp = require('amqplib');

(async function () {
  try {
    const conn = await amqp.connect('amqp://root:root@localhost');
    const ch = await conn.createChannel();
    const q = 'hello';
    const msg = 'Hello World!';

    ch.assertQueue(q, { durable: false });
    ch.sendToQueue(q, Buffer.from(msg));
    console.log(" [x] Sent %s", msg);

    let graceful = true;
    process.on('SIGINT', async () => {
      if (graceful) {
        graceful = false;
        ch && await ch.close();
        conn && await conn.close();
        process.exit(0);
      } else {
        process.exit(1);
      }
    });
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
})();
