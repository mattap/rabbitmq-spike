#!/usr/bin/env node

const amqp = require('amqplib');

(async function () {
  try {
    const conn = await amqp.connect('amqp://root:root@localhost');
    const ch = await conn.createChannel();
    const q = 'hello';

    ch.assertQueue(q, { durable: false });
    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q);
    ch.consume(q, function(msg) {
      console.log(" [x] Received %s", msg.content.toString());
    }, { noAck: true });

    let graceful = true;
    process.on('SIGINT', async () => {
      if (graceful) {
        graceful = false;
        ch && await ch.close();
        conn && await conn.close();
        process.exit(0);
      } else {
        process.exit(1);
      }
    });
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
})();
